/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * functions-grammar.h
 * Copyright (C) 2018 Konstantin Kozlov <mackoel@gmail.com>
 *
 * nlreg is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * nlreg is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _FUNCTIONS_GRAMMAR_H_
#define _FUNCTIONS_GRAMMAR_H_

// based on github.com:pbharrin/Genetic-Prog.git

#include <string>
#include <vector>
#include <map>

void set_print_trace (int arg);

using namespace std;

class GrammarNode
{
public:
	string label = "GrammarNode";
	int precendence;
	int numChildren;
	GrammarNode* children[3];
	int child_type[3];
	GrammarNode() {
		children[0] = NULL;
		children[1] = NULL;
		children[2] = NULL;
	} // to be used later
	virtual ~GrammarNode() { } // to be used later
	virtual double eval(vector<double>& inVal) = 0;  //setting the 0 makes it a PURE
	virtual void coprint() = 0;
	virtual GrammarNode* prune() = 0;
	virtual GrammarNode* clone() = 0; //make a deep copy of the current tree
	virtual void setScale(vector<double>& a, vector<double>& b) = 0;
	virtual string getLabel() {	return label; }
// Print function. It's virtual, so only one operator << is needed.
	virtual void printOn (ostream& os) { os << label; }
};

// Print operator for all classes that inherit from GrammarNode
inline ostream& operator << (ostream& os, GrammarNode& gnd)
{
  gnd.printOn (os);
  return os;
}

//class for storing constant values
class ConstNode : public GrammarNode {
	double *constVal;
	int constInd;
public:
	ConstNode();
	ConstNode(int preSetVal);
	ConstNode(int ind, double *preSetVal);
	virtual double eval(vector<double>& inVal);
	ConstNode* clone();
	virtual void coprint();
	virtual ConstNode* prune();
	virtual void setScale(vector<double>& a, vector<double>& b) { }
//	virtual string getLabel();
//	virtual void printOn (ostream& os) { os << "Const" << endl; };
};

//class for using inputs
class InputNode : public GrammarNode {
	int inputIndex;
	string inpname;
	bool scalePresent;
	double scale;
	double offset;
public:
	InputNode ();
	InputNode(int inputId, string pname);
	virtual double eval(vector<double>& inVal);
	InputNode* clone();
	virtual void coprint();
	virtual InputNode* prune();
	virtual void setScale(vector<double>& a, vector<double>& b) {
		if (inputIndex >= 0) {
			scale = a[inputIndex]; offset = b[inputIndex]; scalePresent = true;
		} else if (children[0]) {
			children[0]->setScale(a, b);
		}
	}
//	void setValues(int inIndex);
//	virtual string getLabel();
//	virtual void printOn (ostream& os) { os << "Input" << endl; };
};

//addition
class Add : public GrammarNode {
public:
	Add();
	virtual double eval(vector<double>& inVal);
	Add* clone();
	virtual void coprint();
	virtual Add* prune();
	virtual void setScale(vector<double>& a, vector<double>& b) {
		if (children[0]) children[0]->setScale(a, b);
		if (children[1]) children[1]->setScale(a, b);
	}
//	virtual string getLabel();
//	virtual void printOn (ostream& os) { os << "Add" << endl; };
};

//subtraction
class Subtract : public GrammarNode {
public:
	Subtract();
	virtual double eval(vector<double>& inVal);
	Subtract* clone();
	virtual void coprint();
	virtual Subtract* prune();
	virtual void setScale(vector<double>& a, vector<double>& b) {
		if (children[0]) children[0]->setScale(a, b);
		if (children[1]) children[1]->setScale(a, b);
	}
//	virtual string getLabel();
//	virtual void printOn (ostream& os) { os << "Subtract" << endl; };
};

//multiplication
class Multiply : public GrammarNode {
public:
	Multiply();
	virtual double eval(vector<double>& inVal);
	Multiply* clone();
	virtual void coprint();
	virtual Multiply* prune();
	virtual void setScale(vector<double>& a, vector<double>& b) {
		if (children[0]) children[0]->setScale(a, b);
		if (children[1]) children[1]->setScale(a, b);
	}
//	virtual string getLabel();
//	virtual void printOn (ostream& os) { os << "Multiply" << endl; };
};

//division
class Divide : public GrammarNode {
public:
	Divide();
	virtual double eval(vector<double>& inVal);
	Divide* clone();
	virtual void coprint();
	virtual Divide* prune();
	virtual void setScale(vector<double>& a, vector<double>& b) {
		if (children[0]) children[0]->setScale(a, b);
		if (children[1]) children[1]->setScale(a, b);
	}
//	virtual string getLabel();
//	virtual void printOn (ostream& os) { os << "Divide" << endl; };
};

//subtraction of const
class InputMinusConst : public GrammarNode {
public:
	InputMinusConst();
	virtual double eval(vector<double>& inVal);
	InputMinusConst* clone();
	virtual void coprint();
	virtual InputMinusConst* prune();
	virtual void setScale(vector<double>& a, vector<double>& b) {
		if (children[0]) children[0]->setScale(a, b);
		if (children[1]) children[1]->setScale(a, b);
	}
};

//subtraction from const
class ConstMinusInput : public GrammarNode {
public:
	ConstMinusInput();
	virtual double eval(vector<double>& inVal);
	ConstMinusInput* clone();
	virtual void coprint();
	virtual ConstMinusInput* prune();
	virtual void setScale(vector<double>& a, vector<double>& b) {
		if (children[0]) children[0]->setScale(a, b);
		if (children[1]) children[1]->setScale(a, b);
	}
	//	virtual string getLabel();
	//	virtual void printOn (ostream& os) { os << "InputMinusConst" << endl; };
};

//rec subtraction of const
class RecInputMinusConst : public GrammarNode {
public:
	RecInputMinusConst();
	virtual double eval(vector<double>& inVal);
	RecInputMinusConst* clone();
	virtual void coprint();
	virtual RecInputMinusConst* prune();
	virtual void setScale(vector<double>& a, vector<double>& b) {
		if (children[0]) children[0]->setScale(a, b);
		if (children[1]) children[1]->setScale(a, b);
	}
};

//rec subtraction from const
class RecConstMinusInput : public GrammarNode {
public:
	RecConstMinusInput();
	virtual double eval(vector<double>& inVal);
	RecConstMinusInput* clone();
	virtual void coprint();
	virtual RecConstMinusInput* prune();
	virtual void setScale(vector<double>& a, vector<double>& b) {
		if (children[0]) children[0]->setScale(a, b);
		if (children[1]) children[1]->setScale(a, b);
	}
};

//second degree
class Sqr : public GrammarNode {
public:
	Sqr();
	virtual double eval(vector<double>& inVal);
	Sqr* clone();
	virtual void coprint();
	virtual Sqr* prune();
	virtual void setScale(vector<double>& a, vector<double>& b) {
		if (children[0]) children[0]->setScale(a, b);
	}
};

//root
class Sqrt : public GrammarNode {
public:
	Sqrt();
	virtual double eval(vector<double>& inVal);
	Sqrt* clone();
	virtual void coprint();
	virtual Sqrt* prune();
	virtual void setScale(vector<double>& a, vector<double>& b) {
		if (children[0]) children[0]->setScale(a, b);
	}
};

//Sin
class Sin : public GrammarNode {
public:
	Sin();
	virtual double eval(vector<double>& inVal);
	Sin* clone();
	virtual void coprint();
	virtual Sin* prune();
	virtual void setScale(vector<double>& a, vector<double>& b) {
		if (children[0]) children[0]->setScale(a, b);
	}
};

//Cos
class Cos : public GrammarNode {
public:
	Cos();
	virtual double eval(vector<double>& inVal);
	Cos* clone();
	virtual void coprint();
	virtual Cos* prune();
	virtual void setScale(vector<double>& a, vector<double>& b) {
		if (children[0]) children[0]->setScale(a, b);
	}
};

//Log
class Log : public GrammarNode {
public:
	Log();
	virtual double eval(vector<double>& inVal);
	Log* clone();
	virtual void coprint();
	virtual Log* prune();
	virtual void setScale(vector<double>& a, vector<double>& b) {
		if (children[0]) children[0]->setScale(a, b);
	}
};

//Exp
class Exp : public GrammarNode {
public:
	Exp();
	virtual double eval(vector<double>& inVal);
	Exp* clone();
	virtual void coprint();
	virtual Exp* prune();
	virtual void setScale(vector<double>& a, vector<double>& b) {
		if (children[0]) children[0]->setScale(a, b);
	}
};

// Multiply on const
class InputMultConst : public GrammarNode {
public:
	InputMultConst();
	virtual double eval(vector<double>& inVal);
	InputMultConst* clone();
	virtual void coprint();
	virtual InputMultConst* prune();
	virtual void setScale(vector<double>& a, vector<double>& b) {
		if (children[0]) children[0]->setScale(a, b);
		if (children[1]) children[1]->setScale(a, b);
	}
};

// Max
class Max : public GrammarNode {
public:
	Max();
	virtual double eval(vector<double>& inVal);
	Max* clone();
	virtual void coprint();
	virtual Max* prune();
	virtual void setScale(vector<double>& a, vector<double>& b) {
		if (children[0]) children[0]->setScale(a, b);
		if (children[1]) children[1]->setScale(a, b);
	}
};

// Min
class Min : public GrammarNode {
public:
	Min();
	virtual double eval(vector<double>& inVal);
	Min* clone();
	virtual void coprint();
	virtual Min* prune();
	virtual void setScale(vector<double>& a, vector<double>& b) {
		if (children[0]) children[0]->setScale(a, b);
		if (children[1]) children[1]->setScale(a, b);
	}
};

// Weighted Average
class WA : public GrammarNode {
public:
	WA();
	virtual double eval(vector<double>& inVal);
	WA* clone();
	virtual void coprint();
	virtual WA* prune();
	virtual void setScale(vector<double>& a, vector<double>& b) {
		if (children[0]) children[0]->setScale(a, b);
		if (children[1]) children[1]->setScale(a, b);
		if (children[2]) children[2]->setScale(a, b);
	}
};

// Ordered Weighted Average
class OWA : public GrammarNode {
public:
	OWA();
	virtual double eval(vector<double>& inVal);
	OWA* clone();
	virtual void coprint();
	virtual OWA* prune();
	virtual void setScale(vector<double>& a, vector<double>& b) {
		if (children[0]) children[0]->setScale(a, b);
		if (children[1]) children[1]->setScale(a, b);
		if (children[2]) children[2]->setScale(a, b);
	}
};

class GrammarContainer {
	
	std::map<string, int> func_codes_1 = {
		{"INPUTNODE", 0},
		{"ADD", 100},
		{"SUBTRACT", 100},
		{"MULTIPLY", 100},
		{"DIVIDE", 100},
		{"INPUTMINUSCONST", 100},
		{"CONSTMINUSINPUT", 100},
		{"RECINPUTMINUSCONST", 100},
		{"RECCONSTMINUSINPUT", 100},
		{"INPUTMULTCONST", 100},
		{"SQR", 100},
		{"SQRT", 100},
		{"SIN", 100},
		{"COS", 100},
		{"LOG", 100},
		{"EXP", 100},
		{"Max", 100},
		{"Min", 100},
		{"WA", 100},
		{"OWA", 100}
	};

	std::map<int, string> func_codes_2 = {
		{0, "INPUTNODE"},
		{1, "ADD"},
		{2, "SUBTRACT"},
		{3, "MULTIPLY"},
		{4, "DIVIDE"},
		{5, "INPUTMINUSCONST"},
		{6, "CONSTMINUSINPUT"},
		{7, "RECINPUTMINUSCONST"},
		{8, "RECCONSTMINUSINPUT"},
		{9, "INPUTMULTCONST"},
		{10, "SQR"},
		{11, "SQRT"},
		{12, "SIN"},
		{13, "COS"},
		{14, "LOG"},
		{15, "EXP"},
		{16, "Max"},
		{17, "Min"},
		{18, "WA"},
		{19, "OWA"}
	};
	

public:
	GrammarContainer (vector<string>& measurements, int n_t, std::vector<int> *use_func = NULL, std::vector<int> *exclude_func = NULL) : predictors(measurements), tree(n_t) {
		n_trees = n_t;
		n_predictors = predictors.size();
		
		n_nodes_type_0 = 20;
		
		e_func = exclude_func;
		u_func = use_func;
		
		if(u_func != NULL && u_func->size() != 0){
			
			for(int i = 0; i < u_func->size(); i++){
				auto search = func_codes_2.find(u_func->at(i));
				if (search != func_codes_2.end()) {
					func_codes_1[search->second] = i + 1;
				}
			}
			n_nodes_type_0 = u_func->size() + 1;
		}
		else{
			for(int i = 0; i < func_codes_2.size(); i++){
				func_codes_1[func_codes_2[i]] = i;
			}
			if(e_func != NULL && e_func->size() != 0){
				for(int i = 0; i < e_func->size(); i++){
					auto search = func_codes_2.find(e_func->at(i));
					if (search != func_codes_2.end()) {
						func_codes_1[search->second] = 100;
					}
				}
				int k = 0;
				for(int i = 0; i < func_codes_1.size(); i++){
					if(func_codes_1[func_codes_2[i]] != 100){
						func_codes_1[func_codes_2[i]] = i - k;
					}
					else{
						k++;
					}
				}
				n_nodes_type_0 -= e_func->size();
			}
		}
		
		/**
		std::cout << n_nodes_type_0 << "\n";
		for(std::map<string, int>::const_iterator it = func_codes_1.begin(); it != func_codes_1.end(); ++it) {
			std::cout << it->first << " " << it->second << "\n";
		}
		**/

		
		n_nodes_type_1 = 1;
		n_nodes_type_2 = 1;
	}
	void build_nth_tree(vector<int>& genotype, vector<double>& conc, int n, double *phenotype, int *phenomask);

	GrammarNode*get_nth_tree(int n) { return tree[n]; }
private:
	vector<string> predictors;
	vector<GrammarNode*> tree;
	int n_trees;
	std::vector<int> *e_func;
	std::vector<int> *u_func;
	int n_predictors;
	int n_nodes_type_0;
	int n_nodes_type_1;
	int n_nodes_type_2;
	int last_predictor;
	int last_const;
	GrammarNode* build_tree(vector<int>& genotype, vector<double>& conc, double *phenotype, int *phenomask, std::map<string, int> funcCodes);
	GrammarNode* find_node(int type, int gen, double conc, double *phenotype, int& phenomask, std::map<string, int> funcCodes);
	GrammarNode* find_node_type_0(int gen, double *phenotype, std::map<string, int> funcCodes);
	GrammarNode* find_node_type_1(int gen, double conc, double *phenotype);
	GrammarNode* find_node_type_2(int gen, double *phenotype);
};

#endif // _FUNCTIONS_GRAMMAR_H_
