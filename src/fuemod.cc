/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * main.cc
 * Copyright (C) 2018 kkozlov <mackoel@gmail.com>
 *
 * nlreg is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * nlreg is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>
#include <iostream>
#include <QtCore>

// #include <omp.h>
#include "kern.h"

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);
	QCommandLineParser parser;
	parser.addHelpOption();
	parser.addVersionOption();
	parser.addOptions({
/*        // A boolean option with a single name (-p)
	{"p",
	QCoreApplication::translate("main", "Show progress during copy")},*/
	// A boolean option with multiple names (-f, --force)
	{{"c", "log-course"},
		QCoreApplication::translate("main", "Log course."),
		QCoreApplication::translate("main", "C")},
	{{"d", "num-days"},
		QCoreApplication::translate("main", "Number of days."),
		QCoreApplication::translate("main", "D")},
	{{"f", "number-of-phases"},
		QCoreApplication::translate("main", "Number of phenological phases in the file."),
		QCoreApplication::translate("main", "F")},
	{{"k", "omp-num-threads"},
		QCoreApplication::translate("main", "Number of OMP threads."),
		QCoreApplication::translate("main", "M")},
	{{"n", "number-of-funcs"},
		QCoreApplication::translate("main", "Number of functions."),
		QCoreApplication::translate("main", "N")},
	{{"l", "length-of-word"},
		QCoreApplication::translate("main", "Length of a word to represent one function."),
		QCoreApplication::translate("main", "L")},
	{{"p", "print-trace"},
		QCoreApplication::translate("main", "Print trace."),
		QCoreApplication::translate("main", "P")},
	{{"q", "extra_covar"},
		QCoreApplication::translate("main", "Read binary covariates.")},
	{{"u", "ignore_extra_covar"},
		QCoreApplication::translate("main", "Read binary covariates.")},
	{{"t", "reg-type"},
		QCoreApplication::translate("main", "Regression type."), /* -1 -- ANN, 0 -- func */
		QCoreApplication::translate("main", "T")},
    });
	parser.addPositionalArgument("samplesfile", "The file to open.");
	parser.addPositionalArgument("weatherfile", "The file to open.");
	parser.addPositionalArgument("funcsfile", "The file to read funcs.");
	parser.process(a);
/*
 * run -n 5 -l 12 -q -p chickpea-ssm-snp.h5 ../test1/ank.h5 test-deep-file-bez-cbd
	if (parser.isSet("omp-num-threads")) {
		const QString mParameter = parser.value("omp-num-threads");
		omp_set_num_threads(mParameter.toInt());
//		cout << "Number of threads set to " << mParameter.toInt() << endl;
//		if (!qputenv("OMP_NUM_THREADS", QByteArray::number(mParameter.toInt()))) {
//			cout << "Number of threads coudn't be set!" << endl;
//		}
	}
*/
	const QStringList args = parser.positionalArguments();

    // Task parented to the application so that it
    // will be deleted by the application.
	Kern *kern;

	if(args.size()) {
		QString file_name = args.at(0); // samples
		QString table_name = args.at(1); // weather
		QString funcs_file_name = args.at(2);
		if (parser.isSet ("number-of-funcs") && parser.isSet("length-of-word")) {
			const QString nfParameter = parser.value("number-of-funcs");
			const int nf = nfParameter.toInt();
			if (nf < 0) {
				std::cout << "Bad nf: " + nf;
			}
			const QString wlParameter = parser.value("length-of-word");
			const int wl = wlParameter.toInt();
			if (wl < 0) {
				std::cout << "Bad wl: " + wl;
			}
			const QString pParameter = parser.value("print-trace");
			const int p = pParameter.toInt();
			if (p < 0) {
				std::cout << "Bad p: " + p;
			}
			const QString ndParameter = parser.value("num-days");
			const int nd = ndParameter.toInt();
			if (nd <= 0) {
				std::cout << "Bad nd: " + nd;
			}
			int np = 1;
			if (parser.isSet("number-of-phases")) {
				const QString npParameter = parser.value("number-of-phases");
				np = npParameter.toInt();
				if (np <= 0) {
					std::cout << "Bad np: " + np;
				}
			}
			const QString rtParameter = parser.value("reg-type");
			const int rt = rtParameter.toInt();
			if ((rt != -1) && (rt != 0) && (rt != 1)) {
				std::cout << "Bad rt: " + rt;
			}
			kern = new Kern(file_name, table_name, funcs_file_name, nf, wl, nd, rt, np, parser.isSet("extra_covar"), p);
			kern->set_use_extra_covar (!parser.isSet("ignore_extra_covar"));
			set_print_trace(p);
			const QString cParameter = parser.value("log-course");
			const int c = cParameter.toInt();
			if (c < 0) {
				std::cout << "Bad c: " + c;
			}
			kern->set_log_course (c);
		}
		if (parser.isSet("omp-num-threads")) {
			const QString mParameter = parser.value("omp-num-threads");
			// omp_set_num_threads(mParameter.toInt());
		}
	}
	kern->run_wi();
    return 0;
}

