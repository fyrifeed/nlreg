#include <QSettings>

#include "functions-grammar.h"

using namespace std;
using namespace HighFive;

class Nlreg
{
    public:
	Nlreg(QString func_file_name, std::vector<std::string> meas,  std::vector<std::string> grn, int nF, int wl, int rF, int print_trace = 0, std::vector<int> *use_func = NULL, std::vector<int> *exclude_func = NULL) : funcs_file_name(func_file_name), measurements(meas), gr_names(grn) {
		nFunctions = nF;
		wordLength = wl;
    u_func = use_func;
    e_func = exclude_func;
		num_of_climate_vars = measurements.size();
		num_of_gt_vars = gr_names.size();
		PRINT_TRACE = print_trace;
		read_flag = rF;
		read_genotype();
	}

		double get_func_value(vector<double> clim_arg, vector<double> gt_vars)
		{
			double val = 0;
			GrammarNode *retFtn;
			for (size_t i = 0; i < nFunctions; ++i) {
				retFtn = grc->get_nth_tree(i);
				double fval = retFtn->eval(clim_arg);
				val += beta[i] * fval;
				for (size_t j = 0; j < num_of_gt_vars; ++j) {
					val += beta[i * num_of_gt_vars + j + nFunctions] * gt_vars[j] * fval;
				}
			}
			return val;
		}

		double get_func_value(vector<double> clim_arg)
		{
			double val = 0;
			GrammarNode *retFtn;
			for (size_t i = 0; i < nFunctions; ++i) {
				retFtn = grc->get_nth_tree(i);
				double fval = retFtn->eval(clim_arg);
				val += beta[i] * fval;
			}
			return val;
		}
		double get_nth_func_value(int n, vector<double> clim_arg)
		{
//			double val = 0;
			GrammarNode *retFtn;
			retFtn = grc->get_nth_tree(n);
			double fval = retFtn->eval(clim_arg);
			return fval;
		}

		double get_cbd()
		{
			return CBD;
		}

		double get_l1_pen()
		{
			double val = 0;
			for (size_t i = 0; i < nFunctions; ++i) {
				val += (beta[i] > 0) ? beta[i] : -beta[i];
				for (size_t j = 0; j < num_of_gt_vars; ++j) {
					val += (beta[j + nFunctions] > 0) ? beta[j + nFunctions] : -beta[j + nFunctions];
				}
			}
			return val;
		}

		double get_l2_pen()
		{
			double val = 0;
			for (size_t i = 0; i < nFunctions; ++i) {
				val += beta[i] * beta[i];
				for (size_t j = 0; j < num_of_gt_vars; ++j) {
					val += beta[j + nFunctions] * beta[j + nFunctions];
				}
			}
			return val;
		}

		void nlreg_build()
		{
			GrammarNode *retFtn;
			grc = new GrammarContainer(measurements, nFunctions, u_func, e_func);
			phenotype = new double[nFunctions * wordLength];
			phenomask = new int[nFunctions * wordLength];
			int first = 0, last = wordLength;
			for (int i = 0; i < nFunctions; ++i) {
				std::vector<int> gt = std::vector<int>(genotype.begin() + first, genotype.begin() + last);
				first += wordLength;
				last += wordLength;
				grc->build_nth_tree(gt, climate_var, i, &phenotype[i * wordLength], &phenomask[i * wordLength]);
			}
		}

	void print_trace(int arg) {
		GrammarNode *retFtn;
		size_t i, j;
		if (arg == 0) {
			for (i = 0; i < nFunctions + nFunctions * num_of_gt_vars; ++i) {
				cout << beta[i] << " ";
			}
			cout << endl;
			cout << "F= ";
			int flag = 0;
			for (i = 0; i < nFunctions + nFunctions * num_of_gt_vars; ++i) {
				if (fabs(beta[i]) > 0) {
					if (flag == 1 && beta[i] > 0) cout << "+";
					cout << beta[i];
					j = i;
					if (j < nFunctions) {
						cout << "*";
						retFtn = grc->get_nth_tree (j);
						retFtn->coprint();
					} else if (j >= nFunctions) {
						int jj = j - nFunctions;
						int ii = jj / num_of_gt_vars;
						int kk = jj % num_of_gt_vars;
						cout << "*" << gr_names[kk] << "*" << "f[" << ii << "]";
					}
					flag = 1;
				}
			}
			cout << endl;
			for (size_t i = 0; i < nFunctions * wordLength; ++i) { // + nEcovar
				cout << phenotype[i] << " ";
			}
			cout << endl;
			for (size_t i = 0; i < nFunctions * wordLength; ++i) { // + nEcovar
				cout << phenomask[i] << " ";
			}
			cout << endl;
		} else {
			for (size_t i = 0; i < nFunctions; ++i) {
				retFtn = grc->get_nth_tree(i);
				cout << i << " ";
				retFtn->coprint();
				cout << endl;
			}
		}
	}
    private:
		int num_of_climate_vars; // Number of columns in the weather table == number of consts to read after func's and betas
		int num_of_gt_vars; // Number of genotype data columns (snp or qtl or locations). Number of betas=number of funcs + number of func * number of gt vars
		int nFunctions;
		int wordLength;
    std::vector<int> *u_func;
		std::vector<int> *e_func;
		int PRINT_TRACE = 1;
		int read_flag = -1;
		std::vector<std::string> measurements;
		std::vector<std::string> gr_names;
		GrammarContainer* grc;
		vector<double> climate_var;
		vector<double> beta;
    vector<double> consts;
		double CBD;
		double MB;
		QString funcs_file_name;
		double *phenotype;
		int *phenomask;
		vector<int> genotype;
    
    void read_genotype() {
      QSettings sett(funcs_file_name, QSettings::IniFormat);
			sett.beginGroup("DEEP");
      
      // read X
			int size = sett.beginReadArray("x");
			double arg_cv;
			for (int i = 0; i < size; ++i) {
				sett.setArrayIndex(i);
				int arg;
				arg = sett.value("value").toInt();
				genotype.push_back(arg);
			}
			sett.endArray();

      // read betas
			size = sett.beginReadArray("beta");
			if (size != 0)
			{
				for (int i = 0; i < size; ++i) {
					sett.setArrayIndex(i);
					double arg;
					arg = sett.value("value").toDouble();
					beta.push_back(arg);
				}	
				sett.endArray();
			}
			else
			{	
				sett.endArray();
				double arg;
			        arg = sett.value("beta").toDouble();
			        beta.push_back(arg);
			}
      
      // read consts
      size = sett.beginReadArray("const");
			if (size != 0)
			{
				for (int i = 0; i < size; ++i) {
					sett.setArrayIndex(i);
          double arg;
					arg = sett.value("value").toDouble();
					consts.push_back(arg);
				}	
				sett.endArray();
			}
			else
			{	
				sett.endArray();
				double arg;
        arg = sett.value("const").toDouble();
        consts.push_back(arg);
			}
      climate_var = consts;
			
			sett.endGroup();
    }
};
