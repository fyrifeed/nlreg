/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * task.h
 * Copyright (C) 2018 Konstantin Kozlov <mackoel@gmail.com>
 *
 * nlreg is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * nlreg is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _TASK_H_
#define _TASK_H_

#include <string>
#include <iostream>
#include <QtCore>

#include <highfive/H5Attribute.hpp>
#include <highfive/H5File.hpp>
#include <highfive/H5DataSet.hpp>
#include <highfive/H5DataSpace.hpp>

#include <armadillo>
#include "nlreg.h"

#include <algorithm>
#include <random>

using namespace std;
using namespace HighFive;

class Task : public QObject
{
    Q_OBJECT
private:
	QString h5_file_name;
	int PRINT_TRACE;
	bool extra_covar;
    int number_of_trees;
    double train_ratio;
    unsigned int random_seed;

    std::pair<std::vector<int>, std::vector<int>> train_test_indexes;
    std::vector<std::pair<std::vector<unsigned int>, std::vector<unsigned int>>> oob_indexes;
	std::vector<std::string> measurements;
	std::vector<std::string> species;
	std::vector<std::vector<double> > vec, resp;
	int nSamples;
	int ns;
	int nPredictors;
	int nn;

	std::vector<std::vector<double> > gr_covar;
	std::vector<std::string> gr_names;
	int nGrCovar;

	arma::vec response;

	arma::vec solution;
	Nlreg *nl;

    std::pair<std::vector<int>, std::vector<int>> get_train_test_split_indexes_with_shuffle()
    {
        default_random_engine rand_engine(random_seed);
        vector<int> whole_indexes(nSamples);
        for(int i = 0; i < nSamples; i++)
        {
            whole_indexes[i] = i;
        }
        std::shuffle(whole_indexes.begin(), whole_indexes.end(), rand_engine);
        int separator = (int) (whole_indexes.size() * train_ratio);
        std::vector<int> train_indexes(whole_indexes.begin(), whole_indexes.begin() + separator),
                test_indexes(whole_indexes.begin() + separator, whole_indexes.end());
        return std::make_pair(train_indexes, test_indexes);
    }

	double compute_score(arma::vec& solution, std::vector<int> indexes) {
		double error = 0;
        int cur_data_size = indexes.size();
        for (size_t j = 0; j < cur_data_size; j++) {
            double cur_func_value = nl->get_func_value(vec[indexes[j]]);
            solution(indexes[j]) = cur_func_value;
            error += pow(solution(indexes[j]) - response[indexes[j]], 2);
        }

		return sqrt(error);
	}
    std::pair<std::vector<unsigned int>, std::vector<unsigned int>> get_bootstrapped_train_test(unsigned int seed)
    {
        std::default_random_engine rand_engine(seed);
        std::vector<unsigned int> entries(nSamples), bootstrapped_train(nSamples);
        std::vector<unsigned int> test;
        for(int i = 0; i < nSamples; i++)
        {
            entries[i] = 0;
        }
        for(int i = 0; i < nSamples; i++)
        {
            bootstrapped_train[i] = rand_engine() % nSamples;
            entries[bootstrapped_train[i]] = 1;
        }

        for(int i = 0; i < nSamples; i++)
        {

            if(entries[i] == 0)
            {
                test.push_back(i);
            }
        }
        return std::make_pair(bootstrapped_train, test);
    }


    std::vector<std::pair<std::vector<unsigned int>, std::vector<unsigned int>>> bootstrap_indexes(int num_of_sets)
    {
        int i;
        std::default_random_engine rand_engine(random_seed);
        std::vector<unsigned int> seeds(num_of_sets);
        for(i = 0; i < num_of_sets; i++)
        {
            seeds[i] = rand_engine();
        }
        std::vector<std::pair<std::vector<unsigned int>, std::vector<unsigned int>>> result;
        std::vector<int> whole_indexes(nSamples);
        for(i = 0; i < nSamples; i++)
        {
            whole_indexes[i] = i;
        }
        for(i = 0; i < num_of_sets; i++)
        {
            result.push_back(get_bootstrapped_train_test(seeds[i]));
        }
        return result;
    }

    double compute_oob_score(std::vector<std::pair<std::vector<unsigned int>, std::vector<unsigned int>>> bootstraped_indexes)
    {
        double current_score = 0;
        int current_index = 0;
        double cur_func_value = 0;
        double total_score = 0;
        //number_of_trees
        for(int i = 0; i < number_of_trees; i++)
        {
            vector<unsigned int> current_test_indexes = bootstraped_indexes[i].second;
            int cur_data_size = current_test_indexes.size();
            for (size_t j = 0; j < cur_data_size; j++)
            {
                current_index = current_test_indexes[j];
                cur_func_value = nl->get_nth_func_value(i, vec[current_index]);
                current_score += pow(cur_func_value - response[current_index], 2);
            }
            total_score += sqrt(current_score);
            current_score = 0;
        }
        return total_score / number_of_trees;
    }

	arma::mat std2arvec(std::vector<std::vector<double> > &vec, int n_rows, int offset) {
		arma::vec Y(n_rows, 1);
		for (size_t i = 0; i < n_rows; ++i) {
			Y(i) = vec[i][offset];
		}
		return Y;
	}

public:
	Task(QString file_name, QString func_file_name, int nF, int wL, bool ecovar = false, int print_trace = 0, double train_r = 0.8, unsigned int rand_seed = 42, QObject *parent = 0, std::vector<int> *use_func = NULL, std::vector<int> *exclude_func = NULL) : QObject(parent), h5_file_name(file_name) {
		PRINT_TRACE = print_trace;
		extra_covar = ecovar;

        random_seed = rand_seed;

        number_of_trees = nF;

        train_ratio = train_r;
// Load data
		nSamples = 100;
		ns = -1;
		nn = -1;
		nGrCovar = 0;
	    try {
		    File file(h5_file_name.toStdString(), File::ReadOnly);
			DataSet a0_read = file.getDataSet("data");
			DataSpace space = a0_read.getSpace();
			nSamples = space.getDimensions()[0]; // number of samples
			nPredictors = space.getDimensions()[1]; // number of measurements
			a0_read.read(vec);
			DataSet b_read = file.getDataSet("response");
			space = b_read.getSpace();
			ns = space.getDimensions()[0]; // number of samples
			nn = space.getDimensions()[1]; // number of measurements
			b_read.read(resp);
			assert(ns == nSamples);
			assert(nn == 1);
			DataSet a1_read = file.getDataSet("species");
			a1_read.read(species);
			DataSet a2_read = file.getDataSet("measurements");
			a2_read.read(measurements);
			if (extra_covar) {
				DataSet c0_read = file.getDataSet("gr_covar");
				DataSpace space = c0_read.getSpace();
				nGrCovar = space.getDimensions()[1]; // number of measurements
				c0_read.read(gr_covar);
				DataSet c1_read = file.getDataSet("gr_names");
				c1_read.read(gr_names);
			}
	    } catch (Exception& err) {
    		std::cerr << err.what() << std::endl;
		}
        oob_indexes = bootstrap_indexes(nF);
        train_test_indexes = get_train_test_split_indexes_with_shuffle();

		response = std2arvec(resp, ns, 0);
		nl = new Nlreg(func_file_name, measurements, gr_names, nF, wL, 1, print_trace, use_func, exclude_func);
		nl->nlreg_build();
	}
public slots:
    void run_wi()
    {
// Train coefficients
		solution = arma::vec(nSamples);
        bool is_test_required = train_ratio == 1;
		double train_error, test_error;
		train_error = compute_score(solution, train_test_indexes.first);
        if (!is_test_required) {
            test_error = compute_score(solution, train_test_indexes.second);
        }

        double test_oob_score = compute_oob_score(oob_indexes);

// Print results
        if (PRINT_TRACE > 0) {
            for (size_t j = 0; j < nSamples; j++) {
                cout << j << " " << response(j) << " " << solution(j) << " " << endl;
            }
        }
        cout << "test_oob_error=" << test_oob_score << endl;

		cout << "train_error=" << train_error << endl;
        if (!is_test_required) cout << "test_error=" << test_error << endl;
		cout << "l1pen=" << nl->get_l1_pen() << endl;
		cout << "l2pen=" << nl->get_l2_pen() << endl;

		emit finished();
	}
signals:
	void finished();
};

#endif // _TASK_H_
