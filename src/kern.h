/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * task.h
 * Copyright (C) 2018 Konstantin Kozlov <mackoel@gmail.com>
 *
 * nlreg is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * nlreg is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _KERN_H_
#define _KERN_H_

#include <string>
#include <iostream>
#include <QtCore>

#include <highfive/H5Attribute.hpp>
#include <highfive/H5File.hpp>
#include <highfive/H5DataSet.hpp>
#include <highfive/H5DataSpace.hpp>

#include <armadillo>
#include "data.h"
#include "nlreg.h"

using namespace std;
using namespace HighFive;

class Kern {
private:
	QString h5_file_name;
	QString h5_table_name;
	QString out_file_name = "out1.csv";
	int PRINT_TRACE;
	int LOG_COURSE;
	int nFunctions;
	int nPhases;
	bool extra_covar;
	bool use_extra_covar;
	int nDays;
	arma::mat Fpoints;
	int reg_type; /* -1 -- ANN, 0 -- func */
	arma::vec solution;
	Nlreg *nl;
	Data Trial;
	double Heaviside(double arg) { return (arg > 0) ? 1.0 : 0.0; }
public:
	Kern(QString file_name, QString table_name, QString func_file_name, int nF, int wL, int nD, int rT, int nP, bool ecovar = false, int print_trace = 0) : h5_file_name(file_name), h5_table_name(table_name) {
/*		std::cout << "Got " << h5_file_name.toStdString() << " file" << std::endl;
		std::cout << "Got " << func_file_name.toStdString() << " funcs" << std::endl;*/
		PRINT_TRACE = print_trace;
		LOG_COURSE = 0;
		nFunctions = nF;
		nDays = nD;
		reg_type = rT;
		extra_covar = ecovar;
		use_extra_covar = ecovar;
// Load data
		nPhases = nP;
		if (nPhases > 1) {
			Trial.read_spieces(h5_file_name, extra_covar, nPhases);
		} else {
			Trial.read_spieces(h5_file_name, extra_covar);
		}
// Load weather data
		Trial.read_h5(h5_table_name);
		nl = new Nlreg(func_file_name, Trial.data_h5.clim_names, Trial.data_a5.gr_names, nF, wL, reg_type, print_trace);
		nl->nlreg_build();
	}
	void set_log_course(int arg) {
		LOG_COURSE = arg;
	}
	void set_use_extra_covar(bool arg) {
		use_extra_covar = arg;
	}
	void run_wi() {
		if (reg_type == -1) run_wi_ann();
		if (reg_type == 0 || reg_type == 1) run_wi_fun();
	}
    void run_wi_ann()
    {
//		int nPhases = Trial.data_m5.nResp;
//		Fpoints = arma::mat(Trial.data_a5.nSamples * nDays, nPhases * (nFunctions + 2) + 1);
		Fpoints = arma::mat(Trial.data_a5.nSamples * nDays, nFunctions + 3);
		double curr_inputs[nFunctions];
		for (size_t nsam = 0; nsam < Trial.data_a5.nSamples; ++nsam) {
			int start_day = Trial.data_a5.doy[nsam];
			int start_year = Trial.data_a5.years[nsam];
			int geo_id = Trial.data_a5.geo_id[nsam];
			double event_day = Trial.data_a5.response[nsam]; // number of days to the event from start day
			size_t j = 0;
			for(size_t nd = 0; nd < Trial.data_h5.nWeather; ++nd) {
				if (geo_id == Trial.data_h5.geo_id[nd] && Trial.data_h5.doy[nd] == start_day && Trial.data_h5.years[nd] == start_year) {
					j = nd;
					break;
				}
			}
			for(size_t k = 0; k < nFunctions; ++k) curr_inputs[k] = 0;
			for(size_t nd = 0; nd < nDays; ++nd) {
				vector<double> clim_covar = {Trial.data_h5.tmax[j + nd], Trial.data_h5.tmin[j + nd], Trial.data_h5.rain[j + nd], Trial.data_h5.dl[j + nd], Trial.data_h5.srad[j + nd]};
				for(size_t k = 0; k < nFunctions; ++k) {
					double arg = nl->get_nth_func_value(k, clim_covar);
					curr_inputs[k] += Heaviside(arg) * arg;
				}
				for(size_t k = 0; k < nFunctions; ++k) Fpoints(nDays * nsam + nd, k) = curr_inputs[k];
				double rhs = 0.1 * ((double)nd + 6.0 - event_day);
				rhs = (rhs > 0.9) ? 0.9 : rhs;
				rhs = (rhs < 0.1) ? 0.1 : rhs;
				Fpoints(nDays * nsam + nd, nFunctions) = rhs;
				Fpoints(nDays * nsam + nd, nFunctions + 1) = (double)nsam;
				Fpoints(nDays * nsam + nd, nFunctions + 2) = (double)event_day;
			}
		}

		QTemporaryFile temp_file;
		if (PRINT_TRACE > 0) {
			temp_file.setAutoRemove(false);
		}
		if (temp_file.open()) {
			out_file_name = temp_file.fileName();
			temp_file.close();
		}
		Trial.write_csv(out_file_name, Fpoints, nFunctions, nDays);
// Train coefficients
		double training_error;
		QProcess training_process;
		training_process.start("./py_model_wrapper", QStringList() << "-a" << "2" << "-b" << QString::number(nFunctions + Trial.data_a5.nGrCovar +2) << out_file_name);
		if (!training_process.waitForStarted())
			training_error = 1e10;
		if (!training_process.waitForFinished(-1))
			training_error = 3e10;
		QByteArray result = training_process.readAll();
/*		cout << result.toStdString();
		cout << result[1];
		cout << result[2];*/
		result.chop(1);
		training_error = result.toDouble();
// Print results
		cout << "terror=" << training_error << endl;
		if (PRINT_TRACE > 0) {
			nl->print_trace(1);
		}
	}
    void run_wi_fun()
    {
		double phase_change = nl->get_cbd();
		double training_error = 0;
		double curr_error = 0;
		int n_success = 0;
		if (LOG_COURSE > 0) {
			cout << "nsam" << " " << "start_day" << " " << "start_year" << " " << "geo_id" << " " << "nd" << " " << "event_day" << " ";
			cout << "tmax" << " " << "tmin" << " " << "rain" << " " << "dl" << " " << "srad" << " ";
			cout << "curr_input" << " " << "arg" << " " << "phase_change" << endl;
		}
		if (use_extra_covar) {
			for (size_t nsam = 0; nsam < Trial.data_a5.nSamples; ++nsam) {
				int start_day = Trial.data_a5.doy[nsam];
				int start_year = Trial.data_a5.years[nsam];
				int geo_id = Trial.data_a5.geo_id[nsam];
				double event_day = Trial.data_a5.response[nsam]; // number of days to the event from start day
				size_t j = 0;
				for(size_t nd = 0; nd < Trial.data_h5.nWeather; ++nd) {
					if (geo_id == Trial.data_h5.geo_id[nd] && Trial.data_h5.doy[nd] == start_day && Trial.data_h5.years[nd] == start_year) {
						j = nd;
						break;
					}
				}
				double curr_input = 0;
				int curr_day = -nDays;
				for(size_t nd = 0; nd < nDays; ++nd) {
					if (j + nd >= Trial.data_h5.nWeather)
						break;
					vector<double> clim_covar = {Trial.data_h5.tmax[j + nd], Trial.data_h5.tmin[j + nd], Trial.data_h5.rain[j + nd], Trial.data_h5.dl[j + nd], Trial.data_h5.srad[j + nd]};
/* Normal operation */
					double arg = nl->get_func_value(clim_covar, Trial.data_a5.gr_covar[nsam]);
/* Test operation */
//					double arg = nl->get_func_value_test(clim_covar, Trial.data_a5.gr_covar[nsam]);
					curr_input += Heaviside(arg) * arg;
					if (LOG_COURSE > 0) {
						cout << nsam << " " << start_day << " " << start_year << " " << geo_id << " " << nd << " " << event_day << " ";
						cout << Trial.data_h5.tmax[j + nd] << " " << Trial.data_h5.tmin[j + nd] << " " << Trial.data_h5.rain[j + nd] << " " << Trial.data_h5.dl[j + nd] << " " << Trial.data_h5.srad[j + nd] << " ";
						cout << curr_input << " " << arg << " " << phase_change << endl;
					}
					if (curr_input >= phase_change) {
						curr_day = nd + 1;
						n_success++;
						break;
					}
				}
				training_error += (curr_day - event_day) * (curr_day - event_day);
				curr_error += (curr_input - phase_change) * (curr_input - phase_change);
				if (PRINT_TRACE > 0) {
					cout << nsam << " " << event_day << " " <<  curr_day << " " <<  curr_input << " " << training_error << " " << curr_error << endl;
				}
			}
		} else {
			for (size_t nsam = 0; nsam < Trial.data_a5.nSamples; ++nsam) {
				int start_day = Trial.data_a5.doy[nsam];
				int start_year = Trial.data_a5.years[nsam];
				int geo_id = Trial.data_a5.geo_id[nsam];
				double event_day = Trial.data_a5.response[nsam]; // number of days to the event from start day
				size_t j = 0;
				for(size_t nd = 0; nd < Trial.data_h5.nWeather; ++nd) {
					if (geo_id == Trial.data_h5.geo_id[nd] && Trial.data_h5.doy[nd] == start_day && Trial.data_h5.years[nd] == start_year) {
						j = nd;
						break;
					}
				}
				double curr_input = 0;
				int curr_day = -nDays;
				for(size_t nd = 0; nd < nDays; ++nd) {
					if (j + nd >= Trial.data_h5.nWeather)
						break;
					vector<double> clim_covar = {Trial.data_h5.tmax[j + nd], Trial.data_h5.tmin[j + nd], Trial.data_h5.rain[j + nd], Trial.data_h5.dl[j + nd], Trial.data_h5.srad[j + nd]};
					double arg = nl->get_func_value(clim_covar);
					curr_input += Heaviside(arg) * arg;
					if (LOG_COURSE > 0) {
						cout << nsam << " " << start_day << " " << start_year << " " << geo_id << " " << nd << " " << event_day << " ";
						cout << Trial.data_h5.tmax[j + nd] << " " << Trial.data_h5.tmin[j + nd] << " " << Trial.data_h5.rain[j + nd] << " " << Trial.data_h5.dl[j + nd] << " " << Trial.data_h5.srad[j + nd] << " ";
						cout << curr_input << " " << arg << " " << phase_change << endl;
					}
					if (curr_input >= phase_change) {
						curr_day = nd + 1;
						n_success++;
						break;
					}
				}
				training_error += (curr_day - event_day) * (curr_day - event_day);
				curr_error += (curr_input - phase_change) * (curr_input - phase_change);
				if (PRINT_TRACE > 0) {
					cout << nsam << " " << event_day << " " <<  curr_day << " " << curr_input << " " << training_error << " " << curr_error << endl;
				}
			}
		}
// Print results
		cout << "terror=" << training_error << endl;
		cout << "cerror=" << curr_error << endl;
		cout << "l1pen=" << nl->get_l1_pen() << endl;
		cout << "l2pen=" << nl->get_l2_pen() << endl;
		cout << "n_success= " << n_success << endl;
		cout << "r_success= " << (double)n_success/(double)Trial.data_a5.nSamples << endl;
		if (PRINT_TRACE > 0) {
			nl->print_trace(0);
		}
	}
};

#endif
